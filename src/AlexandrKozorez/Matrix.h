﻿#pragma once
#include "matrix_1xM.h"

class matrix {
	
	// Ввод и вывод в поток матрицы
	friend istream& operator>>(istream &stream, matrix&matr);
	friend ostream& operator<<(ostream&stream, matrix&matr);

	// Перегрузка == и !=
	friend bool operator ==(const matrix& left, const matrix& right);
	friend bool operator!=(const matrix& left, const matrix& right);

	private:
		matrix_1xM * matr;
		int cols, rows;
		string name;
		bool isOk;
	public:

		// Конструктор с параметрами, по умолчанию, копирования, деструктор
		matrix(int cols, int rows, string name)
		{
			this->name = name;
			this->isOk = cols&&rows ? isOk = true : isOk = false;
			this->cols = cols;
			this->rows = rows;
			matr = new matrix_1xM[cols];
			for (int i = 0;i < cols;i++)
				matr[i] = matrix_1xM(rows);
		}
		matrix()
		{
			this->name = "NULL";
			this->isOk = false;
			this->cols = 1;
			this->rows = 1;
			matr = new matrix_1xM[1];
			matr[0][0] = 0;
		}
		matrix(const matrix& m)
		{
			this->name = m.name;
			this->matr = m.matr;
			this->cols = m.cols;
			this->rows = m.rows;
			this->isOk = m.isOk;
		}
		~matrix(){}
		// Вывод в cout, заполнение случайными числами, геттеры размеров, сетер имени
		void print()
		{
			cout << "\n Матрица "<< this->name << ":\n";
			for (int i = 0;i < cols;i++)
			{
				for (int j = 0;j < rows;j++)
					cout << setw(8) << matr[i][j];
				cout << "\n";
			}
		}
		void randomize(int n, int m)
		{
			bool check = (n<=cols && n >= 0 ) && (m<=rows && m>=0);
			if (check)
			{
				for (int i = 0;i < n;i++)
					for (int j = 0;j < m;j++)
					matr[i][j] = rand() % 9 + 1;
			}
			else
			{
				cout << " \nerror.log.1: Wrong array size";
			}
		}
		int N() const { return cols; }
		int M() const { return rows; }
		void setName(string name) { this->name = name; }
		string getName() { return name; }
		// Операции между матрицей и матрицей
		matrix operator+(const matrix &m) const
		{
			if (isOk && m.isOk)
				if (cols == m.N() && rows == m.M())
				{
					matrix tmp(cols, rows, name + "+" + m.name);
					for (int i = 0;i < cols;i++)
						for (int j = 0;j < cols;j++)
							tmp[i][j] = this->matr[i][j] + m.matr[i][j];
					return tmp;
				}
				else 
				{
					cout << "\n error.log.1: error(+):\n Размеры матриц "<< this->name << " и "<< m.name <<" не совпадают\n";
					return matrix();
				}
			else
			{
				cout << "\n error.log.1: error(+):\n Ошибка: как минимум одна из матриц пуста\n";
				return matrix();
			}
		}
		matrix operator-(const matrix &m) const
		{
			if (isOk && m.isOk)
				if (cols == m.N() && rows == m.M())
				{
					matrix tmp(cols, rows, name + "-" + m.name);
					for (int i = 0;i < cols;i++)
						for (int j = 0;j < cols;j++)
							tmp[i][j] = this->matr[i][j] - m.matr[i][j];
					return tmp;
				}
				else
				{
					cout << "\n error.log.2: error(-):\n Размеры матриц " << this->name << " и " << m.name << " не совпадают\n";
					return matrix();
				}
			else
			{
				cout << "\n error.log.2: error(-):\n Ошибка: как минимум одна из матриц пуста\n";
				return matrix();
			}
		}
		matrix operator*(const matrix &m) const
		{
			if (rows != m.cols)
			{
				cout << "\n error.log.3: error(*):\n Число столбцов " << name << " != числу строк " << m.name << "\n";
				return matrix();
			}
			matrix tmp(cols, m.rows, name + "*" + m.name);
			for (int i = 0;i < rows;i++)
				for (int j = 0;j < m.cols;j++)
					for (int k = 0;k < m.cols;k++)
						tmp[i][j] += (matr[i][j] * m.matr[k][j]);
			return tmp;
		}

		// Операции между матрицей и числом
		matrix operator+(int const x)  
		{
			matrix tmp(*this);
			for (int i = 0;i < cols;i++)
				for (int j = 0;j < rows;j++)
					tmp[i][j]+= x;
			return tmp;
		}
		matrix operator-(int const x) 
		{
			matrix tmp(*this);
			for (int i = 0;i < cols;i++)
				for (int j = 0;j < rows;j++)
					tmp[i][j] -= x;
			return tmp;
		}
		matrix operator*(int const x) 
		{
			matrix tmp(*this);
			for (int i = 0;i < cols;i++)
				for (int j = 0;j < rows;j++)
					tmp[i][j] *= x;
			return tmp;
		}
		matrix operator/(int const x) 
		{
			if (x == 0)
			{
				cout << "\n error.log.4: error(/):\n Деление матрицы на ноль\n";
				return matrix();
			}
			matrix tmp(*this);
			for (int i = 0;i < cols;i++)
				for (int j = 0;j < rows;j++)
					tmp[i][j] /= x;
			return tmp;
		}

		// Операция присваивания и совмещения
		const matrix& operator=(matrix const &m)
		{
			if (&m != this)
			{
				if (cols != m.cols && rows != m.rows)
				{
					delete[] matr;
					cols = m.cols;
					matr = new matrix_1xM[cols];
					for (int i = 0;i < cols;i++)
						matr[i] = matrix_1xM(rows);
				}
				for (int i = 0;i < cols;i++)
					for (int j = 0;j < rows;j++)
						matr[i][j] = m.matr[i][j];
			}
			return *this;
		}
		matrix& operator+=(const matrix &m) 
		{
			matrix tmp = *this;
			tmp = tmp + m;
			return tmp;
		}
		matrix& operator+=(int const x) 
		{
			matrix tmp = *this;
			tmp = tmp - x;
			return tmp;
		}
		matrix& operator-=(const matrix &m) 
		{
			matrix tmp = * this;
			tmp = tmp - m;
			return tmp;
		}
		matrix& operator-=(int const x) 
		{
			matrix tmp = *this;
			tmp = tmp - x;
			return tmp;
		}
		matrix& operator*=(const matrix &m) 
		{
			matrix tmp = *this;
			tmp = tmp * m;
			return tmp;
		}
		matrix& operator*=(int const x) 
		{
			matrix tmp = *this;
			tmp = tmp * x;
			return tmp;
		}
		matrix& operator/=(int const x) 
		{
			matrix tmp = *this;
			tmp = tmp / x;
			return tmp;
		}
		// Операция индексации
		matrix_1xM& operator[](int i)
		{
			return matr[i];
		}

		// Методы 
		void getMinor(matrix cur, matrix& tmp, int i, int j)
		{
			int ki = 0, kj = 0, di = 0, dj = 0, m = cur.rows;
			for (ki = 0;ki < m - 1;ki++)
			{
				if (ki == i) di = 1;
				dj = 0;
				for (kj = 0;kj < m - 1;kj++)
				{
					if (kj == j)dj = 1;
					tmp[ki][kj] = cur[ki + di][kj + dj];
				}
			}
		}
		double det()
		{
			if (isOk && (rows == cols))
			{
				if (rows == 1)
					return matr[0][0];
				else if (rows == 2)
					return matr[0][0] * matr[1][1] - matr[0][1] * matr[1][0];
				else{
					int j = 0, i, k = 1;
					double d = 0.0;
					for (i = 0;i < rows;i++)
					{
						matrix tmp(cols-1, rows-1,""), cur = *this;
						getMinor(cur, tmp, i, j);
						d = d + k * cur[i][0] * tmp.det();
						k = -k;
					}
					return d;
				}
			}
			else
			{
				cout << "\n error.log.5: error(det()):\n Определитель не может быть посчитан для этой матрицы\n";
				return 0;
			} 
		}
		matrix transposeIt()
		{
			int n = M(), m = N();
			matrix tmp(n, m, name);
			for (int i = 0;i < n;i++)
				for (int j = 0;j < m;j++)
					tmp[i][j] = matr[j][i];
			tmp.setName("Т["+tmp.getName()+"]");
			return tmp;
		}
		matrix another()
		{
			matrix tmp = *this;
			int n = tmp.N();
			matrix result(n, n, "(" + tmp.name + "^-1)");
			double d = tmp.det();
			if (d!=0)
			{
				for (int i = 0;i < n;i++)
					for (int j = 0;j < n;j++)
					{
						matrix minor(n-1, n-1, "tmp2");
						getMinor(tmp, minor, i, j);
						result[i][j] = pow(-1.0, i + j + 2)*minor.det() / d;
					}
				return result;
			}
			else
			{
				cout << "\n error.log.6: error(another()):\n Матрица необратима\n";
				return matrix();
			}
		}
		
		matrix readFrom(string PATH)
		{
			ifstream read(PATH);
			if (read.is_open() == false)
			{
				cout << "\n error.log.7: error(readFrom()):\n Чтение из файла невозможно\n";
				exit(EXIT_FAILURE);
			}
			string nn, mm;
			read >> nn >> mm;
			int n = atoi(nn.c_str()), m = atoi(mm.c_str());
			matrix tmp(n, m, "fromFile");
			for (int i = 0;i < n;i++)
				for (int j = 0;j < m;j++)
					read >> tmp[i][j];

			cout << "\n success.log.0: success(readFrom())\n Матрица из файла " << PATH << " была успешно прочитана\n";
			return tmp;
		}
		void writeInto(string PATH)
		{
			ofstream write(PATH);
			if (write.is_open() == false)
			{
				cout << "\n error.log.8: error(writeInto()):\n Запись в файл невозможна\n";
				exit(EXIT_FAILURE);
			}
			write << *this << "\n\n Определитель: " << this->det() << " \n\n " <<
				this->transposeIt() << this->another();

			cout << "\n success.log.1: success(writeInto()) \n Для " << name << " были выведены det(), transpose(), another() в файл: " << PATH << "\n";
		}

};

ostream& operator<<(ostream &stream, matrix&matr)
{
	if (matr.isOk != false)
	{
		int n = matr.N(), m = matr.M();
		stream << "\n Матрица " << matr.name << ":\n";
		for (int i = 0;i < n;i++)
		{
			for (int j = 0;j < m;j++)
				stream << setw(8) << setprecision(3) << matr[i][j];
			stream << "\n";
		}
	}
	return stream;
}
istream& operator>>(istream&stream, matrix&matr)
{
	return stream;
}
bool operator==(matrix const&a,matrix const& b)
{
	if (a.rows != b.rows || a.cols != b.cols) return false;
	for (int i = 0;i < a.rows;i++)
		for (int j = 0;j < a.cols;j++)
			if (a.matr[i][j] != b.matr[i][j]) return false;
	return true;
}
bool operator!=(matrix const& a, matrix const& b) { return !(a == b); }
