#pragma once
#include "stdc++.h"
using namespace std;

class matrix_1xM {

private:
	double* values;
	int size;
	bool isOk;
public:
	matrix_1xM(int size = 0)
	{
		this->isOk = size > 0 ? true : false;
		this->size = size;
		this->values = new double[size];
		for (int i = 0;i < size;i++)
			values[i] = 0;
	}
	~matrix_1xM() {}
	double& operator[](int i)
	{
		return values[i];
	}

	void randomize(int n)
	{
		bool check = n <= size && n >= 0;
		if (check)
		{
			for (int i = 0;i < n;i++)
				values[i] = rand() % 89 + 10;
		}
		else
		{
			cout << " \nerror.log.1: Wrong array size";
		}
	}
};
