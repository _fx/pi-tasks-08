﻿#pragma once

#include "Matrix.h"

int main()
{
	setlocale(LC_ALL, "Russian");
	matrix first(3, 3, "first");
	// Вывод пустой матрицы в cout
	first.print();
	// Метод генерирования эллементов матрицы
	first.randomize(3, 3);

	// Перегрузка индексирования работает на ура
	cout << " Matr[1][1] = " << first[1][1] << " \n";
	first[1][1] = 5;

	// Вывод в поток отформатированного массива
	cout << first;

	// Конструктор копирования
	matrix second(3, 2, "second");
	second.randomize(3, 2);
	cout << second;

	// Перегрузка операций
	matrix a = first*second;
	cout << second + first << first - second << a + 4 << first / 2 << second * 2;

	matrix b(first);
	b -= first;
	cout << b;

	matrix null_matrix(b.N(), b.M(), "null_matrix");
	cout << "\n Матрица " << null_matrix.getName() << " == " << b.getName() << " : "
		<< ((null_matrix == b) ? "true\n" : "false\n");
	cout << " Матрица " << null_matrix.getName() << " != " << b.getName() << " : "
		<< ((null_matrix != b) ? "true\n" : "false\n");

	b.randomize(b.N(), b.M());
	b.setName("b");
	cout << b;
	cout << "\n Определитель: " << b.det() << "\n";
	cout << b.transposeIt() << b.transposeIt();

	cout << b.another();

	matrix* toSomeFunc = new matrix;
	matrix bigNameOfMatrixThatWasReadFromFile = toSomeFunc->readFrom("matrix.in.txt");
	bigNameOfMatrixThatWasReadFromFile.writeInto("matrix.out.txt");

	return 0;
}